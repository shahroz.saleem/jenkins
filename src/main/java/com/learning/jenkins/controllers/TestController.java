package com.learning.jenkins.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@RequestMapping(value = "/evenOrOdd/{number}")
	public String evenOrOdd(@PathVariable int number) {
		return number % 2 == 0 ? "Even" : "Odd";
	}
	
}
